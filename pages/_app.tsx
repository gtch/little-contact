import { AppProps } from 'next/app';
import 'semantic-ui-css/semantic.css';
import '../styles/globals.css';
import { MediaContextProvider } from '../components/media';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <MediaContextProvider>
      <Component {...pageProps} />
    </MediaContextProvider>
  )
}
