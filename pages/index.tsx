import Head from 'next/head';
import styles from 'styles/index.module.css';
import Link from 'next/link';
import { config } from 'config/config';
import Layout from 'components/layout';
import auth from '../components/auth0/auth';
import LoginLogout from '../components/LoginLogout';
import { Button, Header } from 'semantic-ui-react';

export default function Index() {
  const {contacts, description, map, me, name} = config;

  const authenticated = auth.isAuthenticated();

  return (
    <Layout>
      <div className={styles.container}>
        <Head>
          <title>{name}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main className={styles.main}>
          <h1 className={styles.title}>
            Welcome to {name}!
          </h1>

          <p className={styles.description}>{description}</p>

          {authenticated && (
            <div className={styles.grid}>
              <Link href="/community/people"><a className={styles.card}>
                <h3>{contacts.pageName} &rarr;</h3>
                <p>Browse contact details.</p>
              </a></Link>

              {map && (
                <Link href="/community/map"><a className={styles.card}>
                  <h3>{map.pageName} &rarr;</h3>
                  <p>View a map of all contacts.</p>
                </a></Link>
              )}
              <Link href="/community/me"><a className={styles.card}>
                <h3>{me.pageName} &rarr;</h3>
                <p>
                  Update my contact details.
                </p>
              </a></Link>
            </div>
          )}
          {!authenticated && (
            <div className={styles.fullwidth}>
              <p>Please enter your email address. If you're in our community you will receive an email with a verification code that gives you access the {config.name} website.</p>
              <LoginLogout />
            </div>
          )}

          {authenticated && (
              <Header size='medium' className={styles.fullwidth}>You are logged in as {auth.getUserName()}
                <Button floated='right' onClick={auth.logout}>Log out</Button>
              </Header>
          )}

        </main>
      </div>
    </Layout>
  )
}
