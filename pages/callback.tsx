import { Auth0Error } from 'auth0-js';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { Message } from 'semantic-ui-react';
import auth from 'components/auth0/auth';

const Callback = () => {
  const [error, setError] = useState<Auth0Error>();

  const router = useRouter()

  useEffect(() => {
    auth.handleAuthentication()
      .then((result) => {
        if (result.isAuthenticated) {
          router.replace('/community/map');
        } else {
          setError(result.error);
        }
      });
  });

  return (
    <>
      {error && (
        <Message negative>
          <Message.Header>
            Error {error.error}
          </Message.Header>
          <p>
            {error.error_description}
            {error.errorDescription}
          </p>
        </Message>
      )}
      {!error && (
        <div>Logging in...</div>
      )}
    </>
  );
};

export default Callback;
