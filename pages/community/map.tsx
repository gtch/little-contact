import Layout from 'components/layout';
import Head from 'next/head';
import { Button, Card, Container, Dropdown, Header } from 'semantic-ui-react';
import React, { useMemo, useState } from 'react';
import ViewContact from 'components/ViewContact';
import { Contact } from 'types/Contact';
import { LatLng } from 'types/LatLng';
import { GroupedContact, GroupedContactMap } from 'types/GroupedContact';
import { Category, CategoryId } from 'types/Category';
import { CategoryOption } from 'types/CategoryOption';
import MapboxMap from 'components/MapboxMap';
import useContacts from 'components/api/useContacts';
import useCategories from 'components/api/useCategories';
import { config } from 'config/config';
import requireLogin from 'components/auth0/requireLogin';

export default function CommunityMap() {
  return requireLogin(() => {
    const { name, map } = config;

    const [selectedPeople, setSelectedPeople] = useState<Contact[]>([]);
    const [selectedLocation, setSelectedLocation] = useState<LatLng>();
    const [selectedCategory, setSelectedCategory] = useState<Category>();

    const contactRequest = useContacts();
    const categoryRequest = useCategories();

    const contacts: Contact[] = useMemo(
      () => contactRequest.data ? contactRequest.data.contacts : [],
      [contactRequest]
    );

    const groupedContacts = useMemo(() => {
      return Object.values(contacts.reduce((groups, contact) => {
        if (contact.location) {
          const { lat, lng } = contact.location!;
          const locationString = `${lat}-${lng}`;
          const group = groups[locationString];
          if (group) {
            group.contacts = [ ...group.contacts, contact ];
          } else {
            groups[locationString] = {
              location: contact.location,
              contacts: [ contact ]
            }
          }
        }
        return groups;
      }, {} as GroupedContactMap));
    }, [contacts]);

    const categoryNames: Category[] = useMemo(() => {
      if (categoryRequest && !categoryRequest.error && categoryRequest.data) {
        return categoryRequest.data.categories;
      } else {
        return [];
      }
    }, [categoryRequest]);

    const categoryOptions: CategoryOption[] = useMemo(() => {
      if (categoryRequest && !categoryRequest.error && categoryRequest.data) {
        return categoryRequest.data.categories.map((category: Category) => ({
          key: category.id as string,
          value: category.id as string,
          text: category.name
        }));
      } else {
        return [];
      }
    }, [categoryRequest])

    const setCategory = (categoryId?: CategoryId) => {
      if (categoryNames) {
        const matchingCategory = categoryNames.find(categoryName => categoryName.id === categoryId);
        setSelectedCategory(matchingCategory);
      }
    };

    const onLocationSelected = (groupedContact?: GroupedContact) => {
      if (groupedContact) {
        setSelectedPeople( groupedContact.contacts);
        setSelectedLocation(groupedContact.location);
      } else {
        setSelectedPeople([]);
        setSelectedLocation(undefined);
      }
    };

    if (map) {
      return (
        <Layout>
          <Head>
            <title>Map — {name}</title>
            <link href="https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.css" rel="stylesheet" />
          </Head>
          <Header size='small' floated='right'>
            <Dropdown clearable selection placeholder='Everyone' options={categoryOptions} value={selectedCategory ? selectedCategory.id : ''} onChange={(event, data) => setCategory(data.value as CategoryId)}  />
          </Header>
          <Header size='large'>{map?.pageName}</Header>
          <div>
            <MapboxMap groupedContacts={groupedContacts} selectedLocation={selectedLocation} selectedCategory={selectedCategory} onLocationSelected={onLocationSelected} />
          </div>

          <Container className='spaceabove'>
            {selectedPeople && selectedPeople.length > 0 && (
              <Button floated='right' onClick={() => onLocationSelected(undefined)}>Clear</Button>
            )}
            <Card.Group>
              {selectedPeople.map((result: Contact) => (
                <ViewContact key={result.id} contact={result} onAddressClick={() => setSelectedLocation(result && result.location)} />
              ))}
            </Card.Group>
          </Container>

        </Layout>
      )
    } else {
      return <Layout>
        <div>Unknown error</div>
      </Layout>
    }
  })
}
