import Layout from 'components/layout';
import Head from 'next/head';
import Fuse from 'fuse.js';
import React, { useMemo, useState } from 'react';
import { Button, Card, Form, Header, Message } from 'semantic-ui-react';
import { ContactRecord } from 'types/ContactRecord';
import { Category, CategoryId } from 'types/Category';
import { CategoryOption } from 'types/CategoryOption';
import useContacts from 'components/api/useContacts';
import ViewContact from 'components/ViewContact';
import { Contact } from 'types/Contact';
import useCategories from 'components/api/useCategories';
import { config } from 'config/config';
import requireLogin from 'components/auth0/requireLogin';

export default function CommunityPeople() {
  return requireLogin(() => {
    const {name, contacts: contactsConfig} = config;

    const [selectedCategory, setSelectedCategory] = useState<Category>();
    const [search, setSearch] = useState('');

    const isFiltering = selectedCategory || search.trim().length > 0;

    const contactRequest = useContacts();
    const categoryRequest = useCategories();

    const contacts: Contact[] = useMemo(
      () => contactRequest.data ? contactRequest.data.contacts : [],
      [contactRequest]
    );

    const categoryNames: Category[] = useMemo(() => {
      if (categoryRequest && !categoryRequest.error && categoryRequest.data) {
        return categoryRequest.data.categories;
      } else {
        return [];
      }
    }, [categoryRequest]);

    const categoryOptions: CategoryOption[] = useMemo(() => {
      if (categoryRequest && !categoryRequest.error && categoryRequest.data) {
        return categoryRequest.data.categories.map((category: Category) => ({
          key: category.id as string,
          value: category.id as string,
          text: category.name
        }));
      } else {
        return [];
      }
    }, [categoryRequest])

    const loadingError = useMemo(() => {
      if (categoryRequest.error) {
        return `${categoryRequest.error.name} loading categories: ${categoryRequest.error.message}`;
      }
      if (contactRequest.error) {
        return `${contactRequest.error.name} loading contacts: ${contactRequest.error.message}`;
      }
      return undefined;
    }, [categoryRequest, contactRequest]);

    const selectedContacts = useMemo(() => {
      let filteredContacts: Contact[] = contacts;
      if (selectedCategory && selectedCategory.id) {
        filteredContacts = contacts.filter(contact => contact.categories && contact.categories.includes(
          selectedCategory.id));
      }
      if (search && search.trim().length > 0) {
        const options: Fuse.IFuseOptions<Contact> = {
          shouldSort: true,
          threshold: 0.4,
          location: 0,
          distance: 100,
          minMatchCharLength: 2,
          keys: ['email', 'firstName', 'lastName', 'description']
        };
        const fuse = new Fuse(filteredContacts, options);
        const searchResult: Fuse.FuseResult<Contact>[] = fuse.search(search.trim());
        filteredContacts = searchResult.map(sr => sr.item);
      }
      return filteredContacts;
    }, [selectedCategory, search, contacts]);

    const setCategory = (categoryId?: CategoryId) => {
      if (categoryNames) {
        const matchingCategory = categoryNames.find(categoryName => categoryName.id === categoryId);
        setSelectedCategory(matchingCategory);
      }
    };

    const setSearchString = (searchString?: string) => {
      setSearch(searchString || '');
    };

    const clearCategoryAndSearch = () => {
      setCategory(undefined);
      setSearch('');
    };

    return (
      <Layout>
        <Head>
          <title>{contactsConfig.pageName} — {name}</title>
        </Head>

        {loadingError && (
          <Message negative>
            <Message.Header>Error</Message.Header>
            <p>{loadingError}</p>
          </Message>
        )}

        <Form>
          <Form.Group widths='equal'>
            {categoryOptions.length > 0 && (
              <Form.Dropdown clearable selection label='Categories' placeholder='Everyone'
                             options={categoryOptions}
                             value={selectedCategory ? selectedCategory.id : ''}
                             onChange={(event, data) => setCategory(data.value as CategoryId)}/>
            )}
            <Form.Input fluid label='Search' value={search}
                        onChange={(event, data) => setSearchString(data.value)}
                        placeholder='Type to search names...'/>
          </Form.Group>
        </Form>

        <Header size='large'>
          {isFiltering && (
            <Button floated='right' onClick={() => clearCategoryAndSearch()}>Show all</Button>
          )}
          {contactsConfig.pageName}
          <Header.Subheader>
            {selectedCategory && search && (
              <>Category <i>{selectedCategory.name}</i> with search <i>"{search}"</i></>
            )}
            {selectedCategory && !search && (
              <>Category <i>{selectedCategory.name}</i></>
            )}
            {!selectedCategory && search && (
              <>Search <i>"{search}"</i></>
            )}
            {!selectedCategory && !search && (
              <>Everyone</>
            )}
          </Header.Subheader>
        </Header>
        {contactRequest.isLoading && (
          <div>Loading...</div>
        )}
        {!contactRequest.isLoading && selectedContacts && selectedContacts.length === 0 && (
          <>
            <p>
              Sorry, no-one found
              {selectedCategory && (
                <> category <i>{selectedCategory.name}</i></>
              )}
              {search && (
                <> with search <i>{search}</i></>
              )}.
            </p>
            <p><Button size='small' onClick={() => clearCategoryAndSearch()}>Show everyone</Button>
            </p>
          </>
        )}

        <Card.Group>
          {selectedContacts && selectedContacts.map((result: ContactRecord) => (
            <ViewContact key={result.id} contact={result}/>
          ))}
        </Card.Group>

      </Layout>
    )
  });
}
