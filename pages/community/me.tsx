import Layout from '../../components/layout';
import requireLogin from '../../components/auth0/requireLogin';
import { Auth0Error, Auth0UserProfile } from 'auth0-js';
import React, { useEffect, useMemo, useState } from 'react';
import {
  Button,
  Card,
  Checkbox,
  CheckboxProps,
  Confirm,
  Dimmer,
  Dropdown,
  Form,
  Grid,
  Header,
  List,
  ListItem,
  Loader,
  Message,
  Modal,
  ModalActions,
  ModalContent,
} from 'semantic-ui-react';
import ViewContact from '../../components/ViewContact';
import { Auth0ErrorMessage } from '../../components/auth0/Auth0ErrorMessage';
import auth from '../../components/auth0/auth';
import { Category, CategoryId } from '../../types/Category';
import { UserMetadata } from '../../types/UserMetadata';
import { ContactAddress } from '../../types/ContactAddress';
import { Contact } from '../../types/Contact';
import { config } from '../../config/config';
import useCategories from '../../components/api/useCategories';

const australianStates = [
  { key: '', text: '', value: undefined },
  { key: 'VIC', text: 'VIC', value: 'VIC' },
];

export default function CommunityMe() {
  return requireLogin(() => {

    const [error, setError] = useState<Auth0Error>();
    const [userProfile, setUserProfile] = useState<Auth0UserProfile>();

    const [firstName, setFirstName] = useState<string>('');
    const [lastName, setLastName] = useState<string>('');
    const [homePhone, setHomePhone] = useState<string>('');
    const [mobilePhone, setMobilePhone] = useState<string>('');
    const [emailAddress, setEmailAddress] = useState<string>('');

    const [addressStreet1, setAddressStreet1] = useState<string>('');
    const [addressStreet2, setAddressStreet2] = useState<string>('');
    const [addressSuburb, setAddressSuburb] = useState<string>('');
    const [addressState, setAddressState] = useState<string>('');
    const [addressPostcode, setAddressPostcode] = useState<string>('');

    const [categories, setCategories] = useState<CategoryId[]>([]);

    const [description, setDescription] = useState<string>();

    const [showConfirm, setShowConfirm] = useState(false);
    const [showUndo, setShowUndo] = useState(false);

    const [isSaving, setIsSaving] = useState(false);

    const categoryRequest = useCategories();

    const categoryNames: Category[] = useMemo(() => {
      if (categoryRequest && !categoryRequest.error && categoryRequest.data) {
        return categoryRequest.data.categories;
      } else {
        return [];
      }
    }, [categoryRequest]);

    const categoryNameList: string[] = useMemo(() => {
      if (categories !== undefined && categoryNames !== undefined) {
        return categoryNames.filter(o => categories.includes(o.id)).map(o => o.name);
      } else {
        return [];
      }
    }, [categories, categoryNames]);

    const isLoading = !userProfile && !categoryNames;

    const contact: Contact | undefined = userProfile ? {
      id: userProfile.sub,
      firstName: firstName,
      lastName: lastName,
      email: emailAddress,
      phone: homePhone,
      mobile: mobilePhone,
      address: {
        street: addressStreet1,
        suburb: addressSuburb,
        state: addressState,
        postcode: addressPostcode,
      },
      categories: categories,
      categoryNames: categoryNameList,
      description: description
    } : undefined;

    const loadUserProfile = (newProfile: Auth0UserProfile) => {
      /* eslint-disable @typescript-eslint/camelcase */
      const user_metadata = newProfile.user_metadata as UserMetadata;
      const address: ContactAddress = user_metadata && user_metadata.address ? user_metadata.address : {};
      setFirstName(user_metadata.firstName || '');
      setLastName(user_metadata.lastName || '');
      setHomePhone(user_metadata.phone || '');
      setMobilePhone(user_metadata.mobile || '');
      setEmailAddress(newProfile.email || '');
      setAddressStreet1(address.street || '');
      setAddressStreet2(address.street2 || '');
      setAddressSuburb(address.suburb || '');
      setAddressState(address.state || '');
      setAddressPostcode(address.postcode || '');
      setCategories(user_metadata.categories || []);
      setDescription(user_metadata.description || '');

      setUserProfile(newProfile);
    };

    const reloadUserProfile = () => {
      if (userProfile) {
        loadUserProfile(userProfile);
        setShowUndo(false);
      }
    };

    const saveUserProfile = () => {
      setIsSaving(true);
      setShowConfirm(false);
      const userMetadata: UserMetadata = {
        firstName: firstName || undefined,
        lastName: lastName || undefined,
        email: emailAddress || undefined,
        phone: homePhone || undefined,
        mobile: mobilePhone || undefined,
        address: {
          street: addressStreet1 || undefined,
          street2: addressStreet2 || undefined,
          suburb: addressSuburb || undefined,
          state: addressState || undefined,
          postcode: addressPostcode || undefined,
        },
        categories: categories ? categories : [],
        description: description || undefined
      };
      auth.updateUserMetadata(userMetadata)
        .then((metadata: Auth0UserProfile) => {
          loadUserProfile(metadata);
          setIsSaving(false);
        })
        .catch((auth0Error: Auth0Error) => {
          setError(auth0Error);
          setIsSaving(false);
        });
    };

    const toggleCategory = (event: React.FormEvent<HTMLInputElement>, data: CheckboxProps) => {
      const categoryId = data.value as CategoryId;
      if (categories) {
        if (data.checked) {
          if (!categories.includes(categoryId)) {
            setCategories([ ...categories, categoryId]);
          }
        } else {
          if (categories.includes(categoryId)) {
            setCategories(categories.filter(category => category !== categoryId));
          }
        }
      }
    };

    useEffect(() => {
      auth.getUserMetadata()
        .then((metadata: Auth0UserProfile) => {
          loadUserProfile(metadata);
        })
        .catch((auth0Error: Auth0Error) => {
          console.log('### error ###', auth0Error);
          setError(auth0Error);
        });

      // fetch('/.netlify/functions/categories', {
      //   headers: {
      //     authorization: `Bearer ${auth.getAccessToken()}`
      //   }
      // })
      //   .then(results => {
      //     if (!results.ok) {
      //       throw Error(results.statusText);
      //     }
      //     return results.json();
      //   })
      //   .then(data => {
      //     setCategoryNames(data.categories);
      //   })
      //   .catch(error => {
      //     console.log('### error ###', error);
      //     setError(error.message);
      //   });
    }, []);

    return (
      <Layout>
        <h1>My address book entry</h1>
        <p>
          This is how you are displayed in the address book. You can make changes below, and your
          details will be updated on the map and address book soon.
        </p>
        {error && (
          <Auth0ErrorMessage error={error} />
        )}
        <div>
          <Grid stackable>
            <Grid.Row>
              <Grid.Column width={6}>
                {isLoading && (
                  <div>
                    <Loader active>Loading</Loader>
                  </div>
                )}
                {!isLoading && contact && (
                  <ViewContact contact={contact} />
                )}
              </Grid.Column>
              <Grid.Column width={10}>
                <Form>

                  <Form.Group widths="equal">
                    <Form.Input fluid disabled={isLoading} label="First name" value={firstName} onChange={(e, data) => setFirstName(data.value)} />
                    <Form.Input fluid disabled={isLoading} label="Last name" value={lastName} onChange={(e, data) => setLastName(data.value)} />
                  </Form.Group>

                  <Form.Input
                    fluid
                    disabled={isLoading}
                    label="Email address"
                    value={emailAddress}
                    onChange={(e, data) => setEmailAddress(data.value)}
                    error={!isLoading && emailAddress !== undefined && emailAddress.trim().length === 0 && 'You must provide an email address'}
                  />

                  <Form.Group widths="equal">
                    <Form.Input fluid disabled={isLoading} label="Home phone" value={homePhone} onChange={(e, data) => setHomePhone(data.value)} />
                    <Form.Input fluid disabled={isLoading} label="Mobile phone" value={mobilePhone} onChange={(e, data) => setMobilePhone(data.value)} />
                  </Form.Group>

                  <Form.Input fluid disabled={isLoading} label="Address" value={addressStreet1} onChange={(e, data) => setAddressStreet1(data.value)} />
                  <Form.Input fluid disabled={isLoading} value={addressStreet2} onChange={(e, data) => setAddressStreet2(data.value)} />
                  <Form.Group>
                    <Form.Input
                      fluid
                      disabled={isLoading}
                      width={8}
                      label="Suburb"
                      value={addressSuburb}
                      onChange={(e, data) => setAddressSuburb(data.value)}
                    />
                    <Form.Field width={4}>
                      <label>State</label>
                      <Dropdown
                        fluid
                        disabled={isLoading}
                        label="State"
                        selection
                        options={australianStates}
                        value={addressState}
                        onChange={(e, data) => setAddressState(data.value as string)}
                      />
                    </Form.Field>
                    <Form.Input
                      fluid
                      disabled={isLoading}
                      width={4}
                      label="Postcode"
                      value={addressPostcode}
                      onChange={(e, data) => setAddressPostcode(data.value)}
                    />
                  </Form.Group>
                  <Header size='medium'>Categories</Header>
                  <p>Tick the box for each category you wish to be included in.</p>
                  {categoryNames && categoryNames.map(category => (
                    <Form.Field key={category.id}>
                      <Checkbox label={category.name} value={category.id} disabled={isLoading} checked={categories && categories.includes(category.id)} onChange={toggleCategory} />
                    </Form.Field>
                  ))}
                  <Form.TextArea
                    label='More details'
                    placeholder={`If you like, tell us something to appear in the ${config.contacts.pageName}`}
                    value={description}
                    onChange={(event, data) => setDescription(data.value as string)}
                  />
                  <Form.Group>
                    <Form.Button disabled={isLoading} primary onClick={() => setShowConfirm(true)}>Save</Form.Button>
                    <Form.Button disabled={isLoading} onClick={() => setShowUndo(true)}>Undo</Form.Button>
                  </Form.Group>

                  <Modal open={showConfirm}>
                    <Header>Confirm changes</Header>
                    <ModalContent>
                      <div>
                        <p>Do you want to save these changes?</p>
                        <Card.Group centered>
                          {contact && (<ViewContact contact={contact} />)}
                          <Card>
                            <Card.Content>
                              <Card.Header>Categories</Card.Header>
                              <Card.Description>
                                <List>
                                  {(categories && categories.length > 0) ? categoryNames && categoryNames.filter(categoryName => categories.includes(categoryName.id)).map(categoryName =>
                                    <ListItem key={categoryName.id}>{categoryName.name}</ListItem>
                                  ) : (
                                    <ListItem key="none">None</ListItem>
                                  )}
                                </List>
                              </Card.Description>
                            </Card.Content>
                          </Card>
                          {description && (
                            <Card>
                              <Card.Content>
                                <Card.Header>More details</Card.Header>
                                <Card.Description>
                                  {description}
                                </Card.Description>
                              </Card.Content>
                            </Card>
                          )}
                        </Card.Group>
                        <Message warning>Note that changes don't immediately appear in the address book, it may take a day or two to update.</Message>
                      </div>
                    </ModalContent>
                    <ModalActions>
                      <Button content='Cancel' onClick={() => setShowConfirm(false)} />
                      <Button primary icon='check' content='Save' onClick={saveUserProfile} />
                    </ModalActions>
                  </Modal>
                  <Confirm
                    open={showUndo}
                    content='Are you sure you want to undo your changes?'
                    onCancel={() => setShowUndo(false)}
                    confirmButton="Undo"
                    onConfirm={reloadUserProfile}
                  />

                </Form>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
        {isSaving && (
          <Dimmer active inverted>
            <Loader>Saving...</Loader>
          </Dimmer>
        )}
      </Layout>
    );

  });
}
