import { NowRequest, NowResponse } from '@now/node';
import { Category } from 'types/Category';
import contactData from '@data/contacts.json';
import categoryData from '@data/categories.json';
import geolocationData from '@data/geolocation.json';
import { Contact } from 'types/Contact';
import { Geolocation } from 'types/Geolocation';
import { ContactRecord } from 'types/ContactRecord';
import { DecodedJwt, verifyJwt } from 'components/auth0/verifyJwt';

export default (req: NowRequest, res: NowResponse) => {

  const categoryDataMap = (categoryData as Category[]).reduce((map: any, category: Category) => {
    return {
      ...map,
      [category.id]: category.name
    }
  }, {});

  const geolocationDataMap: any = (geolocationData as Geolocation[]).reduce((map: any, geolocation: Geolocation) => {
    let {lat, lng, street, suburb} = geolocation;
    return {
      ...map,
      [`${suburb}|${street}`]: { lat, lng }
    }
  }, {});

  const contactsWithCategories: Contact[] = (contactData as ContactRecord[]).map(contact => {
    let {address, categories} = contact;
    const latLng = address && geolocationDataMap[`${address.suburb}|${address.street}`]
    return {
      ...contact,
      ...categories && {
        categoryNames: categories.map(id => categoryDataMap[id])
      },
      ...latLng && {
        location: latLng
      }
    }
  }).sort((a, b) => {
    var nameA = `${a.firstName} ${a.lastName}`.toLowerCase();
    var nameB = `${b.firstName} ${b.lastName}`.toLowerCase();
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }

    // names must be equal
    return 0;
  });


  verifyJwt(req, res, (_jwt: DecodedJwt) => ({
    statusCode: 200,
    jsonBody: {
      contacts: contactsWithCategories
    }
  }));

};
