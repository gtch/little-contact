import categoryData from '@data/categories.json';
import { NowRequest, NowResponse } from '@now/node';
import { DecodedJwt, verifyJwt } from 'components/auth0/verifyJwt';

export default (req: NowRequest, res: NowResponse) => {
  verifyJwt(req, res, (_jwt: DecodedJwt) => ({
    statusCode: 200,
    jsonBody: {
      categories: categoryData
    }
  }));
};
