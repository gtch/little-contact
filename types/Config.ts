import { MapViewport } from './MapViewport';

export type Config = {
  name: string;
  description?: string;
  contacts: PageConfig;
  map?: MapConfig;
  me: PageConfig;
  auth0: Auth0Config;
}

export type PageConfig = {
  linkName: string;
  pageName: string;
}

export type MapConfig = PageConfig & {
  mapboxToken: string;
  mapboxStyle: string;
  viewport: MapViewport;
}

export type Auth0Config = {
  domain: string;
  clientId: string;
  audience: string;
  redirectUri: string;
  publicKey: string;
}
