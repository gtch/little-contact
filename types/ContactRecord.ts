import { LatLng } from './LatLng';
import { CategoryId } from './Category';
import { ContactAddress } from './ContactAddress';

export type ContactRecord = {
  id: string;
  firstName: string;
  lastName: string;
  email?: string;
  phone?: string;
  mobile?: string;
  address?: ContactAddress;
  categories?: CategoryId[];
  location?: LatLng;
};

