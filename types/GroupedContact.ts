import { Contact } from './Contact';
import { LatLng } from './LatLng';

export interface GroupedContact {
  location: LatLng,
  contacts: Contact[]
}

export interface GroupedContactMap {
  [key: string]: GroupedContact
}

