import { ContactRecord } from './ContactRecord';

export type ContactResponse = {
  contacts: ContactRecord[]
}
