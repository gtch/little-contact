export type ContactAddress = {
  street?: string;
  street2?: string;
  suburb?: string;
  state?: string;
  postcode?: string;
  country?: string;
};
