import { ContactRecord } from './ContactRecord';

export type Contact = ContactRecord & {
  categoryNames?: string[];
  description?: string;
}
