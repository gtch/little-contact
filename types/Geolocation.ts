import { LatLng } from './LatLng';

export type Geolocation = LatLng & {
  street: string;
  suburb: string;
};
