export type MapViewport = {
  latitude: number;
  longitude: number;
  zoom: number;
}
