export type ApiResponse = {
  statusCode: number;
  jsonBody: any;
}

