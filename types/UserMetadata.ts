import { ContactAddress } from './ContactAddress';
import { CategoryId } from './Category';

export type UserMetadata = {
  firstName?: string;
  lastName?: string;
  email?: string;
  phone?: string;
  mobile?: string;
  address?: ContactAddress;
  categories?: CategoryId[];
  description?: string;
};
