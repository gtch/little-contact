export type SwrResponse<T> = {
  data?: T;
  error?: Error,
  isLoading: boolean
};
