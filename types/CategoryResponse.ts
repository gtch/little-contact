import { Category } from './Category';

export type CategoryResponse = {
  categories: Category[]
}
