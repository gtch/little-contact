export type CategoryOption = {
  key: string;
  value: string;
  text: string;
}

