export type CategoryId = string;

export type Category = {
  id: CategoryId;
  name: string;
}

