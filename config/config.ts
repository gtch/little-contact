import { Config } from 'types/Config';

export const config: Config = {
  name: "Little Contact",
  description: "This is a demo site for the Little Contact app, a little app for sharing contact details with your family, neighbours or social group.",
  contacts: {
    linkName: "Address book",
    pageName: "Address book"
  },
  map: {
    linkName: "Map",
    pageName: "Example map",
    mapboxToken: "...",
    mapboxStyle: "mapbox://...",
    viewport: {
      latitude: -33.18,
      longitude: 147.89,
      zoom: 5.2
    }
  },
  me: {
    linkName: "Me",
    pageName: "My details"
  },
  auth0: {
    domain: "....au.auth0.com",
    clientId: "...",
    audience: "https://....au.auth0.com/api/v2/",
    redirectUri: "https://....vercel.app/callback",
    publicKey: "-----BEGIN CERTIFICATE-----\n" +
      "....................................\n" +
      "-----END CERTIFICATE-----\n"
  }
}
