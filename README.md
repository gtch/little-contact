# Little Contact

Little Contact is a little app for securely sharing contact details with your family, neighbours or social group.

It is designed to run on a small, low-cost static hosting service like Vercel, and it uses Auth0 to provide secure
passwordless logins — everyone in the address book can log in with just their email address, Auto0 sends a code via
email when someone wants to log in.

Little Contact stores contact data inside your git repo, so make sure you keep your repo private! The idea is that 
contact data isn't largre and it doesn't change very often, so it's quite practical to store it in git. Doing so
avoids expensive database hosting costs, and it maintains a nice change history inside git.

## Getting Started

First up: fork this repo! Little Contact is designed to be run from a private fork because it stores its data inside
the repository itself.

Next up you'll need to set up an account with:

* [Auth0](https://auth0.com/)
* [Vercel](https://vercel.com/)
* [Bitbucket](https://bitbucket.org/)
 
Then edit the `/config/config.ts` file, filling in the `auth0` section with values from the accounts you created above.  


### Running locally

Before deploying, try running the development server locally to confirm everything is set up right:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.


### Deploy on Vercel

Once you're happy with how Little Contact is running locally, deploy it on the 
[Vercel Platform](https://vercel.com/import?utm_medium=default-template&filter=next.js) to make it available
on the internet.

## License

Copyright 2020 Charles Gutjahr.

Little Contact is released under the MIT license. See `LICENSE.md` for details.
