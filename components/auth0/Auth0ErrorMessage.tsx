import { Auth0Error } from 'auth0-js';
import React from 'react';
import { Message } from 'semantic-ui-react';

interface Auth0ErrorMessageProps {
  error: Auth0Error;
}

export const Auth0ErrorMessage: React.FC<Auth0ErrorMessageProps> = ({ error }) => (
  <Message negative>
    <Message.Header>Error ${error.code}</Message.Header>
    <p>{error.name}: {error.description}</p>
  </Message>
);
