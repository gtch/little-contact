import jwt, { VerifyOptions } from 'jsonwebtoken';
import { config } from 'config/config';
import { NowRequest, NowResponse } from '@now/node';
import { ApiResponse } from '../../types/ApiResponse';

export const verifyJwt = (
  req: NowRequest,
  res: NowResponse,
  onVerify: (jwt: DecodedJwt) => ApiResponse
): void => {

  const authorization = req.headers && req.headers.authorization;
  if (!authorization) {
    res.status(401).send('Unauthorised: no Authorization header provided.');
    return;
  }

  const authorizationParts = authorization!.split(' ');
  const bearerPart = authorizationParts[0];
  const tokenPart = authorizationParts[1];
  if (!(bearerPart.toLowerCase() === 'bearer' && !!tokenPart)) {
    res.status(401).send('Unauthorised: Authorization header does not contain a bearer token');
    return;
  }

  const options: VerifyOptions = {
    algorithms: ['RS256'],
    audience: config.auth0.audience
  };

  try {
    // const debugDecode = jwt.decode(tokenPart, { complete: true, json: true });
    const decoded = jwt.verify(tokenPart, config.auth0.publicKey, options) as DecodedJwt;
    const result = onVerify(decoded);
    res.status(result.statusCode).json(result.jsonBody);
    return;
  } catch (err) {
    // 401 Unauthorized
    console.log(`Token invalid: ${err}`);
    res.status(401).send('Unauthorised');
    return;
  }
};

export type DecodedJwt = {
  aud: string;
  exp: number;
  iat: number;
  iss: string;
  sub: string;
  azp?: string;
  gty?: string;
}
