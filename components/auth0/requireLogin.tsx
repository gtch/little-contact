import auth from './auth';
import Link from 'next/link';
import Layout from '../layout';
import { Header } from 'semantic-ui-react';

export default function
  requireLogin(pageElement: () => JSX.Element): JSX.Element {
  if (auth.isAuthenticated()) {
    return pageElement()
  } else {
    return (
      <Layout>
        <Header>Not logged in</Header>
        <p>
          You need to be logged in to access this page.
          Please <Link href="/"><a>go to the homepage to log in</a></Link>.
        </p>
      </Layout>
    )
  }
}
