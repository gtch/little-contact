import auth0, { Auth0Error, Auth0UserProfile } from 'auth0-js';
import { UserMetadata } from 'types/UserMetadata';
import { config } from '../../config/config';
import { Auth0Config } from '../../types/Config';


function userIsInLocalStorage(): boolean {
  return (typeof localStorage !== 'undefined' && !!localStorage.getItem('user'));
}

function loadLocalStorageUser(): Auth0UserProfile | undefined {
  if (userIsInLocalStorage()) {
    return JSON.parse(localStorage.getItem('user')!);
  }
  return undefined;
}


export class Auth {
  private auth0: auth0.WebAuth;
  private domain: string;

  public constructor(auth0Config: Auth0Config) {
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.handleAuthentication = this.handleAuthentication.bind(this);
    this.isAuthenticated = this.isAuthenticated.bind(this);

    this.auth0 = new auth0.WebAuth({
      domain: auth0Config.domain,
      clientID: auth0Config.clientId,
      redirectUri: auth0Config.redirectUri,
      audience: auth0Config.audience,
      responseType: 'token id_token',
      scope: 'openid profile email read:current_user update:current_user_metadata create:current_user_metadata',
    });
    this.domain = auth0Config.domain;
  }

  public login(emailAddress: string, callback: (err: any, res: any) => void) {
    console.log('Logging in with', emailAddress);
    this.auth0.passwordlessStart(
      {
        connection: 'email',
        send: 'code',
        email: emailAddress
      },
      callback
    );
  }

  public verifyCode(emailAddress: string, otpCode: string, callback: (err: any, res: any) => void) {
    console.log(`Verifying code ${otpCode} with ${emailAddress}`);
    this.auth0.passwordlessVerify(
      {
        connection: 'email',
        email: emailAddress,
        verificationCode: otpCode
      },
      callback
    );
  }

  public logout() {
    if (typeof localStorage !== 'undefined') {
      localStorage.removeItem('access_token');
      localStorage.removeItem('id_token');
      localStorage.removeItem('expires_at');
      localStorage.removeItem('user');
    }

    if (typeof window !== 'undefined') {
      this.auth0.logout({
        returnTo: window.location.origin
      });
    }
  }

  public handleAuthentication(): Promise<AuthenticationResult> {
    return new Promise((resolve, reject) => {
      if (typeof window !== 'undefined') {
        this.auth0.parseHash((err: any, authResult: any) => {
          if (authResult && authResult.accessToken && authResult.idToken) {
            this.setSession(authResult);
          } else if (err) {
            console.log(err);
          }

          if (err) {
            if (console && console.warn) console.warn('Error processing login:', err);
            resolve({
              isAuthenticated: false,
              error: err
            });
          } else {
            resolve({
              isAuthenticated: this.isAuthenticated()
            });
          }
        });
      } else {
        reject(new Error('Cannot authenticate because no browser detected'));
      }
    });
  }

  public parseAuthentication(callback: (result: any) => void) {
    if (typeof window !== 'undefined') {
      this.auth0.parseHash((err: any, authResult: any) => {
        if (authResult && authResult.accessToken && authResult.idToken) {
          this.setSession(authResult);
          callback({
            result: 'ok',
            auth: authResult
          });
        } else if (err) {
          console.log(err);
          callback({
            result: 'error',
            auth: authResult,
            error: err
          });
        } else {
          callback({
            result: 'none',
            auth: authResult
          });
        }
      });
    }
  }

  public isAuthenticated(): boolean {
    if (typeof localStorage !== 'undefined') {
      const expiresAt = localStorage.getItem('expires_at');
      return expiresAt ? new Date().getTime() < JSON.parse(expiresAt || '') : false;
    }
    return false;
  }

  public setSession(authResult: any) {
    if (typeof localStorage !== 'undefined') {
      const expiresAt = JSON.stringify(authResult.expiresIn * 1000 + new Date().getTime());
      localStorage.setItem('access_token', authResult.accessToken);
      localStorage.setItem('id_token', authResult.idToken);
      localStorage.setItem('expires_at', expiresAt);

      this.auth0.client.userInfo(authResult.accessToken, (err: any, user: any) => {
        localStorage.setItem('user', JSON.stringify(user));
      });
    }
  }

  public getUser: () => Auth0UserProfile | undefined = loadLocalStorageUser;

  public getUserName(): string | undefined {
    if (userIsInLocalStorage()) {
      return loadLocalStorageUser()!.name;
    }
    return undefined;
  }

  public getAccessToken(): string | undefined {
    if (typeof localStorage !== 'undefined') {
      if (localStorage.getItem('access_token')) {
        return localStorage.getItem('access_token') || undefined;
      }
    }
    return undefined;
  }

  public getUserMetadata(): Promise<Auth0UserProfile> {
    const user = loadLocalStorageUser();
    const token = this.getAccessToken();
    if (user && token) {
      return new Promise<Auth0UserProfile>((resolve, reject) => {
        const management = new auth0.Management({ token, domain: this.domain });
        management.getUser(user.sub, (error: null | Auth0Error, result: Auth0UserProfile) => {
          if (error) {
            reject(error);
          } else {
            resolve(result);
          }
        });
      });
    }
    return Promise.reject(new Error('No access token available to access user metadata'));
  }

  public updateUserMetadata(userMetadata: UserMetadata): Promise<Auth0UserProfile> {
    const user = loadLocalStorageUser();
    const token = this.getAccessToken();
    if (user && token) {
      return new Promise<Auth0UserProfile>((resolve, reject) => {
        const management = new auth0.Management({ token, domain: this.domain });
        management.patchUserMetadata(user.sub, userMetadata, (error: null | Auth0Error, result: Auth0UserProfile) => {
          if (error) {
            reject(error);
          } else {
            resolve(result);
          }
        });
      });
    }
    return Promise.reject(new Error('No access token available to access user metadata'));
  }

  public updateUserAttributes(userMetadata: UserMetadata, firstName?: string, lastName?: string): Promise<Auth0UserProfile> {
    const user = loadLocalStorageUser();
    const token = this.getAccessToken();
    if (user && token) {
      const updatedUser = {
        user_metadata: userMetadata,
        ...firstName && { given_name: firstName },
        ...lastName && { family_name: lastName }
      };
      return new Promise<Auth0UserProfile>((resolve, reject) => {
        const management = new auth0.Management({ token, domain: this.domain });
        management.patchUserAttributes(user.sub, updatedUser as Auth0UserProfile, (error: null | Auth0Error, result: Auth0UserProfile) => {
          if (error) {
            reject(error);
          } else {
            resolve(result);
          }
        });
      });
    }
    return Promise.reject(new Error('No access token available to access user attributes'));
  }
}

export interface AuthenticationResult {
  isAuthenticated: boolean;
  error?: Auth0Error;
}

const auth = new Auth(config.auth0);
export default auth;
