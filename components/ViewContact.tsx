import * as React from 'react';
import { useState } from 'react';
import { Button, Card, List } from 'semantic-ui-react';
import './ViewContact.module.css';
import ViewContactModal from 'components/ViewContactModal';
import { Contact } from '../types/Contact';

type ViewContactProps = {
  contact: Contact;
  onAddressClick?: () => void;
}

const ViewContact: React.FC<ViewContactProps> = ({ contact, onAddressClick }) => {

  const [showDescription, setShowDescription] = useState(false);

  return (
    <Card key={contact.id}>
      <Card.Content>
        <Card.Header onClick={onAddressClick} className="clickable">
          {contact.firstName} {contact.lastName}
        </Card.Header>
        <List>
          {contact.address && (
            <List.Item className="adr clickable" onClick={onAddressClick} >
              <List.Icon name="marker" title="Address" />
              <List.Content>
                <div>
                {contact.address.street && <div className="street-address">{contact.address.street}</div>}
                <div>
                  {contact.address.suburb && <span className="locality">{contact.address.suburb} </span>}
                  {contact.address.state && <span className="region">{contact.address.state} </span>}
                  {contact.address.postcode && <span className="postal-code">{contact.address.postcode} </span>}
                  {contact.address.country && <span className="country-name hidden">{contact.address.country}</span>}
                </div>
                </div>
              </List.Content>
            </List.Item>
          )}
          {contact.email && (
            <List.Item>
              <List.Icon name="mail" />
              <List.Content>
                <a className="email" href={`mailto:${contact.email}`}>
                  {contact.email}
                </a>
              </List.Content>
            </List.Item>
          )}
          {contact.phone && (
            <List.Item>
              <List.Icon name="phone" />
              <List.Content className="tel">
                <span className="type hidden">Home</span>
                <a className="value" href={`tel:${contact.phone}`}>{contact.phone}</a>
              </List.Content>
            </List.Item>
          )}
          {contact.mobile && (
            <List.Item>
              <List.Icon name="mobile alternate" />
              <List.Content className="tel">
                <span className="type hidden">Cell</span>
                <a className="value" href={`tel:${contact.mobile}`}>{contact.mobile}</a>
              </List.Content>
            </List.Item>
          )}
          {(contact.categoryNames || contact.description) && (
            <List.Item>
              <List.Icon name="hand paper" />
              <List.Content>
                {contact.categoryNames && contact.categoryNames.map(categoryName => (
                  <div key={categoryName}>{categoryName}</div>
                ))}
                {contact.description && (
                  <div style={{marginLeft: '-14px', marginTop: '8px'}}>
                    <Button size='tiny' compact onClick={() => setShowDescription(true)}>More information...</Button>
                    <ViewContactModal show={showDescription} contact={contact} onClose={() => setShowDescription(false)} />
                  </div>
                )}
              </List.Content>
            </List.Item>
          )}
        </List>
      </Card.Content>
    </Card>

  )
};

export default ViewContact;

