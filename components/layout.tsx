import { ReactNode } from 'react';
import { Container, Dropdown, Image, Menu } from 'semantic-ui-react';
import Link from 'next/link';
import { Media } from './media';
import layout from './layout.module.css';
import { config } from '../config/config';
import auth from './auth0/auth';

type Props = {
  children?: ReactNode
}

function Layout({ children }: Props) {
  let {contacts, name, map, me} = config;

  const loggedIn = auth.isAuthenticated();

  return (
    <>
      <Menu fixed="top" inverted>
        <Container>
          <Link href="/">
            <Menu.Item header className={layout.headerLink}>
              <Image size="mini" src="/logo.svg" alt="Logo"/>
              <Media greaterThan="mobile">
                <div className={layout.headerTest}>{name}</div>
              </Media>
            </Menu.Item>
          </Link>
          {loggedIn && (
            <>
              <Menu.Item>
                <Link href="/community/people">
                  <a>{contacts.linkName}</a>
                </Link>
              </Menu.Item>
              {map && (
                <Menu.Item>
                  <Link href="/community/map">
                    <a>{map.linkName}</a>
                  </Link>
                </Menu.Item>
              )}
              <Menu.Item>
                <Link href="/community/me">
                  <a>{me.linkName}</a>
                </Link>
              </Menu.Item>
            </>
          )}
        </Container>
      </Menu>

      <Container style={{paddingTop: '80px'}}>
        {children}
      </Container>
    </>
  );
}

export default Layout;
