import React, { useState } from 'react';
import MapGL, { Marker, NavigationControl } from 'react-map-gl';
import { Button } from 'semantic-ui-react';
import { config } from 'config/config';
import { GroupedContact } from 'types/GroupedContact';
import { LatLng } from 'types/LatLng';
import { Category } from 'types/Category';
import { Contact } from 'types/Contact';
import MapContactPopup from './MapContactPopup';
import { MapViewport } from '../types/MapViewport';

interface MarkersProps {
  groupedContacts: GroupedContact[]
  category?: Category
  onClick: (groupedContact: GroupedContact) => void
}

const MarkersInternal = (props: MarkersProps) => {
  const { groupedContacts, category, onClick } = props;
  return (
    <>
      {groupedContacts.map(
        contactGroup => {
          const { lat, lng } = contactGroup.location;
          const active = !category || (contactGroup.contacts.some(contact => contact.categories && contact.categories.includes(category.id)));
          return (
            <Marker
              key={`${lat}-${lng}-${active}`}
              longitude={lng}
              latitude={lat}
              offsetTop={-30}
              offsetLeft={-15}
            >
              {active ? (
                <img src="/images/marker-15.svg" width="30px" alt="Place" onClick={() => onClick(contactGroup)} style={{ cursor: 'pointer' }} />
              ) : (
                <img src="/images/marker-15.svg" width="30px" alt="Place" onClick={() => onClick(contactGroup)} style={{ cursor: 'pointer', opacity: 0.3 }} />
              )}
            </Marker>
          )
        }
      )}
    </>
  )
};

const Markers = React.memo(MarkersInternal) as typeof MarkersInternal;

interface MapboxMapProps {
  groupedContacts?: GroupedContact[]
  selectedLocation?: LatLng
  selectedCategory?: Category
  onLocationSelected: (groupedContact?: GroupedContact) => void
}


const MapboxMap: React.FC<MapboxMapProps> = ({groupedContacts, selectedLocation, selectedCategory, onLocationSelected}) => {
  const { map } = config;

  const [viewport, setViewport] = useState<MapViewport | undefined>(map && map.viewport);

  const navigateToContact = (contact: Contact) => {
    if (contact.location) {
      const { lat, lng } = contact.location;
      setViewport({
        latitude: lat,
        longitude: lng,
        zoom: map!.viewport.zoom
      })
    }
  };

  const navigateHome = () => {
    onLocationSelected(undefined);
    setViewport(map!.viewport)
  };

  // TODO: memoise this to avoid recalculations
  const selectedGroupedContact: GroupedContact | undefined = selectedLocation ? groupedContacts?.find(item => item.location === selectedLocation) : undefined;

  if (map && map.mapboxToken) {
    return (
      <MapGL
        {...viewport}
        bearing={0}
        pitch={0}
        height="70vh"
        width="auto"
        mapStyle={map.mapboxStyle}
        onViewportChange={(nextViewport: MapViewport) => {
          // Uncomment the next line to log the location of the map as you move around (useful for manually geocoding addresses!)
          // console.log(`Viewport lat=${nextViewport.latitude} lng=${nextViewport.longitude} zoom=${nextViewport.zoom}`);
          setViewport(nextViewport)
        }}
        mapboxApiAccessToken={map.mapboxToken}
      >
        <div style={{position: 'absolute', right: '5px', top: '8px'}}>
          <Button color="black" icon="home" onClick={navigateHome} />
        </div>
        <div style={{position: 'absolute', right: '8px', top: '50px'}}>
          <NavigationControl />
        </div>
        {groupedContacts && (
          <Markers groupedContacts={groupedContacts} category={selectedCategory} onClick={groupedContact => selectedLocation === groupedContact.location ? onLocationSelected(undefined) : onLocationSelected(groupedContact)} />
        )}
        {selectedGroupedContact && (
          <MapContactPopup key={`popup-${selectedGroupedContact.location.lat}-${selectedGroupedContact.location.lng}`} groupedContact={selectedGroupedContact} onClose={() => onLocationSelected(undefined)} />
        )}

      </MapGL>
    );
  } else {
    return (
      <div>The map has not been set up. Please configure a MapBox token to enable the map.</div>
    )
  }

};

export default MapboxMap;
