import React, { ChangeEvent, useState } from 'react';
import { Button, Header, Input, Message } from 'semantic-ui-react';
import auth from './auth0/auth';
import Link from 'next/link';
import { config } from '../config/config';

const LoginLogout = () => {
  // export default class LoginLogout extends React.Component<{}, LoginState> {

  const [email, setEmail] = useState('');
  const [otpCode, setOtpCode] = useState('');
  const [emailSent, setEmailSent] = useState(false);
  const [completed, setCompleted] = useState(false);
  const [authenticated, setAuthenticated] = useState(auth.isAuthenticated());
  const [error, setError] = useState<string | undefined>();

  const emailIsValid = email.trim().length > 0;

  const login = () => {
    auth.login(email, (err: any, res: any) => {
      if (err) {
        setError(err.description);
        setEmailSent(false);
      } else {
        setError(undefined);
        setEmailSent(true);
      }
    });
  };

  const verifyCode = () => {
    auth.verifyCode(email, otpCode, (err: any, res: any) => {
      if (err) {
        setError(err.description);
      } else {
        setError(undefined);
        setCompleted(true);
      }
    });
  };

  const logout = () => {
    auth.logout();
    setAuthenticated(auth.isAuthenticated());
    setEmailSent(false);
    setCompleted(false);
  };

  const emailChange = (event: ChangeEvent<HTMLInputElement>) => {
    setEmail((event && event.target && event.target.value) || '');
  };

  const otpCodeChange = (event: ChangeEvent<HTMLInputElement>) => {
    setOtpCode((event && event.target && event.target.value) || '');
  };

  return (
    <>
      {!authenticated && !emailSent && (
        <div>
          {error && (
            <Message negative>
              <Message.Header>Error</Message.Header>
              <p>{error}</p>
            </Message>
          )}
          <Header size='medium'>Email</Header>
          <Input
            action={
              <Button primary onClick={login} disabled={!emailIsValid} size="big">
                Get code
              </Button>
            }
            fluid
            size="big"
            placeholder="Your email address..."
            value={email}
            onChange={emailChange}
          />
        </div>
      )}
      {!authenticated && emailSent && !completed && (
        <>
          <Message positive>
            <Message.Header>Check your email!</Message.Header>
            <p>
              A verification code has been sent to <span>{email}</span>. Please check your email and type in the verification code below:
            </p>
          </Message>
          <Header size='medium'>Code</Header>
          <Input
            action={
              <Button primary onClick={verifyCode} size="big">
                Log in
              </Button>
            }
            fluid
            size="big"
            placeholder="Type in the code that was just emailed to you..."
            value={otpCode}
            onChange={otpCodeChange}
          />
        </>
      )}
      {!authenticated && completed && (
        <>
          <Message positive>
            <Message.Header>Logging in</Message.Header>
            <p>
              Your login has been completed, the map should appear in a moment...
            </p>
          </Message>
          <Button primary onClick={logout} size="big">
            Cancel login
          </Button>
        </>
      )}
      {authenticated && (
        <Message>
          <Message.Header>You're logged in!</Message.Header>
          <p>
            <Link href="/community/contacts">Go to {config.contacts.pageName}</Link> to see contact details.
          </p>
        </Message>
      )}
    </>
  );
};

export default LoginLogout;
