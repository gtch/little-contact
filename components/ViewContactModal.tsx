import * as React from 'react';
import { Button, Header, List, Modal } from 'semantic-ui-react';
import { Contact } from '../types/Contact';

interface ViewContactModalProps {
  show: boolean;
  contact: Contact;
  onClose: () => void;
}

const ViewContactModal: React.FC<ViewContactModalProps> = ({ show, contact, onClose }) => (
  <Modal basic size='small' open={show}>
    <Header>{contact.firstName} {contact.lastName}</Header>
    <Modal.Content>
      <List>
        {contact.address && (
          <List.Item className="adr">
            <List.Icon name="marker" title="Address" />
            <List.Content>
              <div>
                {contact.address.street && <div className="street-address">{contact.address.street}</div>}
                <div>
                  {contact.address.suburb && <span className="locality">{contact.address.suburb} </span>}
                  {contact.address.state && <span className="region">{contact.address.state} </span>}
                  {contact.address.postcode && <span className="postal-code">{contact.address.postcode} </span>}
                  {contact.address.country && <span className="country-name hidden">{contact.address.country}</span>}
                </div>
              </div>
            </List.Content>
          </List.Item>
        )}
        {contact.email && (
          <List.Item>
            <List.Icon name="mail" />
            <List.Content>
              <a className="email whitelink" href={`mailto:${contact.email}`}>
                {contact.email}
              </a>
            </List.Content>
          </List.Item>
        )}
        {contact.phone && (
          <List.Item>
            <List.Icon name="phone" />
            <List.Content className="tel">
              <span className="type hidden">Home</span>
              <a className="value whitelink" href={`tel:${contact.phone}`}>{contact.phone}</a>
            </List.Content>
          </List.Item>
        )}
        {contact.mobile && (
          <List.Item>
            <List.Icon name="mobile alternate" />
            <List.Content className="tel">
              <span className="type hidden">Cell</span>
              <a className="value whitelink" href={`tel:${contact.mobile}`}>{contact.mobile}</a>
            </List.Content>
          </List.Item>
        )}
        {contact.categoryNames && contact.categoryNames.map(
          categoryName => (
            <div key={categoryName}>{categoryName}</div>
          )
        )}
        {contact.description && (
          <>
            <h4>More information:</h4>
            <p>{contact.description}</p>
          </>
        )}
      </List>
    </Modal.Content>
    <Modal.Actions>
      <Button basic inverted onClick={onClose}>
        OK
      </Button>
    </Modal.Actions>
  </Modal>

);

export default ViewContactModal;
