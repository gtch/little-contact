import React, { useState } from 'react';
import { Popup } from 'react-map-gl';
import { Header } from 'semantic-ui-react';
import ViewContactModal from './ViewContactModal';
import { GroupedContact } from '../types/GroupedContact';
import { Contact } from '../types/Contact';

interface MapContactPopupProps {
  groupedContact: GroupedContact;
  onClose: () => void;
}

const MapContactPopup: React.FC<MapContactPopupProps> = ({ groupedContact, onClose }) => {
  const [displayContact, setDisplayContact] = useState<Contact>();

    const { contacts, location } = groupedContact;
    const { lat, lng } = location;
    return (
      <Popup
        latitude={lat}
        longitude={lng}
        closeButton={true}
        closeOnClick={false}
        onClose={onClose}
        anchor="top" >
        <Header size="medium" floated='left'>
          {contacts.map(contact => (
            <React.Fragment key={contact.id}>
              <div onClick={() => setDisplayContact(contact)} className='clickable'>
                {contact.firstName} {contact.lastName}
              </div>
              <ViewContactModal show={!!displayContact && displayContact === contact} contact={contact} onClose={() => setDisplayContact(undefined)} />
            </React.Fragment>
          ))}
          {contacts && contacts[0].address && (
            <Header.Subheader>
              {contacts[0].address.street}
            </Header.Subheader>
          )}
        </Header>
      </Popup>
    );
};

export default MapContactPopup;
