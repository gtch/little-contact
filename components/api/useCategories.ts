import useSWR from 'swr';
import { SwrResponse } from '../../types/SwrResponse';
import { CategoryResponse } from '../../types/CategoryResponse';
import { fetcher } from './fetcher';

export default function useCategories(): SwrResponse<CategoryResponse> {
  const { data, error } = useSWR(`/api/categories`, fetcher)
  return {
    error,
    data,
    isLoading: !error && !data,
  }
}
