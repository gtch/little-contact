import auth from '../auth0/auth';

export const fetcher = (input: RequestInfo, init?: RequestInit) => fetch(input, {
  ...init,
  headers: {
    authorization: `Bearer ${auth.getAccessToken()}`
  }
}).then(res => res.json())
