import useSWR from 'swr';
import { SwrResponse } from '../../types/SwrResponse';
import { ContactResponse } from '../../types/ContactResponse';
import { fetcher } from './fetcher';

export default function useContacts(): SwrResponse<ContactResponse> {
  const { data, error } = useSWR(`/api/contacts`, fetcher)
  return {
    error,
    data,
    isLoading: !error && !data,
  }
}
